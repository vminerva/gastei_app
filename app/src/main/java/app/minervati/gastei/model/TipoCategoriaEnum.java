package app.minervati.gastei.model;

/**
 * Created by victorminerva on 20/03/2016.
 */
public enum TipoCategoriaEnum {

    A("ALIMENTAÇÃO"),
    C("COMPRAS"),
    CT("CONTAS"),
    E("EDUCAÇÃO"),
    L("LAZER"),
    M("MERCADO"),
    O("OFICINA"),
    S("SAÚDE");

    private final String valor;

    TipoCategoriaEnum(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

}
