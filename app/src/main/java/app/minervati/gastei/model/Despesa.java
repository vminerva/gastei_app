package app.minervati.gastei.model;

/**
 * Created by victorminerva on 20/03/2016.
 */
public class Despesa {

    private String      mId;
    private String      descricao;
    private Double      valor;
    private Categoria   categoria;

    public Despesa(String descricao, Double valor, Categoria categoria){
        this.descricao  = descricao;
        this.valor      = valor;
        this.categoria  = categoria;
    }

    public String getmId() {
        return mId;
    }
    public void setmId(String mId) {
        this.mId = mId;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }
    public Categoria getCategoria() {
        return categoria;
    }
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
