package app.minervati.gastei.model;

/**
 * Created by victorminerva on 20/03/2016.
 */
public class Categoria {

    private final   String              mId;
    private         int                 photoId;
    private         TipoCategoriaEnum   tipo;

    private final   Theme               theme;

    public Categoria(String id, int photoId, TipoCategoriaEnum tipo, Theme theme){
        this.mId        = id;
        this.tipo       = tipo;
        this.photoId    = photoId;
        this.theme      = theme;
    }

    /**
     * GETTER's AND SETTER's
     */
    public String getId() {
        return mId;
    }
    public int getPhotoId() {
        return photoId;
    }
    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
    public TipoCategoriaEnum getTipo() {
        return tipo;
    }
    public void setTipo(TipoCategoriaEnum tipo) {
        this.tipo = tipo;
    }

    public Theme getTheme() {
        return theme;
    }

    /**
     * EQUALS AND HASHCODE
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Categoria categoria = (Categoria) o;

        if (photoId != categoria.photoId) return false;
        if (!mId.equals(categoria.mId)) return false;
        if (!tipo.equals(categoria.tipo)) return false;
        return !(theme != null ? !theme.equals(categoria.theme) : categoria.theme != null);

    }

    @Override
    public int hashCode() {
        int result = mId.hashCode();
        result = 31 * result + photoId;
        result = 31 * result + tipo.hashCode();
        result = 31 * result + (theme != null ? theme.hashCode() : 0);
        return result;
    }

}
