package app.minervati.gastei.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import app.minervati.gastei.R;
import app.minervati.gastei.adapter.HomeCategoryAdapter;
import app.minervati.gastei.model.Categoria;
import app.minervati.gastei.model.Despesa;
import app.minervati.gastei.model.Theme;
import app.minervati.gastei.model.TipoCategoriaEnum;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends Fragment {

    /**
     * Variaveis da tela
     */
    private Spinner         mes_spinner;
    private RecyclerView    recyclerViewHome;

    /**
     * Adapters
     */
    private ArrayAdapter<CharSequence> adapter;
    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    List<Categoria>     categorias;
    List<Despesa>       despesas;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(int num) {
        HomeFragment f = new HomeFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initComponents(view);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mes_spinner.setAdapter(adapter);

        recyclerViewHome.setHasFixedSize(true);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerViewHome.setLayoutManager(staggeredGridLayoutManager);

        initializeData();
        initializeAdapter();

        return view;
    }

    public void initComponents(View view){
        mes_spinner = (Spinner) view.findViewById(R.id.mes_spinner);
        adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.meses_array, android.R.layout.simple_spinner_item);
        recyclerViewHome = (RecyclerView) view.findViewById(R.id.categories);
    }

    private void initializeAdapter() {
        mAdapter = new HomeCategoryAdapter(categorias, getActivity());
        recyclerViewHome.setAdapter(mAdapter);
    }

    //Teste
    private void initializeData(){
        categorias  = new ArrayList<>();
        despesas    = new ArrayList<>();

        categorias.add(new Categoria("1",R.drawable.image_category_compras, TipoCategoriaEnum.C, Theme.pink));
        categorias.add(new Categoria("2",R.drawable.image_category_saude, TipoCategoriaEnum.S, Theme.blue));
        categorias.add(new Categoria("3", R.drawable.image_category_food, TipoCategoriaEnum.A, Theme.green));
        categorias.add(new Categoria("4",R.drawable.image_category_entertainment, TipoCategoriaEnum.L, Theme.grey));
        categorias.add(new Categoria("5",R.drawable.image_category_automovel, TipoCategoriaEnum.O, Theme.purple));
        categorias.add(new Categoria("6",R.drawable.image_category_contas, TipoCategoriaEnum.CT, Theme.topeka));
        categorias.add(new Categoria("7",R.drawable.image_category_knowledge, TipoCategoriaEnum.E, Theme.yellow));
        categorias.add(new Categoria("8",R.drawable.image_category_mercado, TipoCategoriaEnum.M, Theme.red));


        for (Categoria categoria : categorias) {
            if(TipoCategoriaEnum.A.getValor().equals(categoria.getTipo()))
              despesas.add(new Despesa("McDonalds", 30.00, categoria));
            if(TipoCategoriaEnum.E.getValor().equals(categoria.getTipo()))
                despesas.add(new Despesa("Faculdade", 800.00, categoria));
            if(TipoCategoriaEnum.L.getValor().equals(categoria.getTipo()))
                despesas.add(new Despesa("Cinema", 20.00, categoria));
        }
    }
}
