package app.minervati.gastei.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.minervati.gastei.R;
import app.minervati.gastei.activity.CategoriaActivity;
import app.minervati.gastei.activity.ManterDespesaActivity;

/**
 * A placeholder fragment containing a simple view.
 */
public class CategoriaFragment extends Fragment/* implements View.OnClickListener*/ {

    public CategoriaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_categoria, container, false);
    }

    /*@Override
    public void onClick(View v) {
        Intent i = new Intent(getActivity(), CategoriaActivity.class);
        //i.putExtra("Category", )
        //TRANSITIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), null);

            getActivity().startActivity(i, options.toBundle());
        } else {
            getActivity().startActivity(i);
        }
    }*/
}
