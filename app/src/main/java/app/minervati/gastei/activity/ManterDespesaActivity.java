package app.minervati.gastei.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fabtransitionactivity.SheetLayout;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.minervati.gastei.R;
import app.minervati.gastei.util.GasteiUtil;

public class ManterDespesaActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final String     CAMPO_OBRIGATORIO = "Este campo é obrigatório.";
    /**
     * Variaveis da tela
     */
    private Spinner                 categorySpinner;
    private EditText                descricao;
    private EditText                value;
    private EditText                data;
    private Button                  saveDespesa;
    private DatePickerDialog        datePickerDialog;
    //private FloatingActionButton    fabSave;
    private CheckedTextView         chkRepetir;
    private AlertDialog.Builder     popDialog;
    private SeekBar                 qtdMesesSeek;
    private TextView                resultMeses;
    /**
     * Adapters
     */
    private ArrayAdapter<CharSequence> adapter;
    /**
     * Variaveis do sistema
     */
    private Calendar            calendar;
    private TransitionInflater  inflater;
    private Transition          transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter_despesa);
        init();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle("Cadastrar nova");

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapter);
        
        descricao.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (GasteiUtil.isNullOrEmpty(String.valueOf(descricao.getText())))
                    descricao.setError(CAMPO_OBRIGATORIO);
            }
        });
        value.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (GasteiUtil.isNullOrEmpty(String.valueOf(value.getText())))
                    value.setError(CAMPO_OBRIGATORIO);
            }
        });
        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog().show(getFragmentManager(), "DatePicker");
            }
        });
        chkRepetir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogQtdMeses(chkRepetir, false);
            }
        });
        saveDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GasteiUtil.isNullOrEmpty(String.valueOf(descricao.getText())))
                    descricao.setError(CAMPO_OBRIGATORIO);
                if(GasteiUtil.isNullOrEmpty(String.valueOf(value.getText())))
                    value.setError(CAMPO_OBRIGATORIO);
                if(!GasteiUtil.isNullOrEmpty(String.valueOf(descricao.getText()))
                    && !GasteiUtil.isNullOrEmpty(String.valueOf(value.getText()))
                        && !GasteiUtil.isNullOrEmpty(data.getText())){
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String valorData = dateFormat.format(calendar.getTime());
                    data.setText(valorData);
                }
            }
        });
    }
    public void init(){
        categorySpinner     = (Spinner) findViewById(R.id.category_spinner);
        adapter             = ArrayAdapter.createFromResource(this,
                                R.array.category_array, android.R.layout.simple_spinner_item);
        descricao           = (EditText) findViewById(R.id.descricao);
        value               = (EditText) findViewById(R.id.value);
        data                = (EditText) findViewById(R.id.data);
        calendar            = Calendar.getInstance();
        saveDespesa         = (Button) findViewById(R.id.save_despesa);
        chkRepetir          = (CheckedTextView) findViewById(R.id.chk_repetir);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //TODO
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
        }
        return true;
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    //Mostra um picker de data.
    private DatePickerDialog dateDialog() {
        Calendar cDefault = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(
                this,
                cDefault.get(Calendar.YEAR),
                cDefault.get(Calendar.MONTH),
                cDefault.get(Calendar.DAY_OF_MONTH));
        return datePickerDialog;
    }

    /**
     * Seta a data informada no datepicker
     */
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        calendar.set(year, monthOfYear, dayOfMonth);
        String valorData = dateFormat.format(calendar.getTime());
        data.setText(valorData);
    }

    private void showDialogQtdMeses(final CheckedTextView chkRepetir, final Boolean isShow){
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.qtd_meses_slider, null);

        resultMeses = (TextView) alertLayout.findViewById(R.id.result_meses);
        qtdMesesSeek = (SeekBar) alertLayout.findViewById(R.id.qtd_meses);
        qtdMesesSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                resultMeses.setText(String.valueOf(progress));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        popDialog = new AlertDialog.Builder(this);
        popDialog.setView(alertLayout)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                chkRepetir.setChecked(isShow);
                                chkRepetir.setTextColor(Color.parseColor("#000000"));
                                chkRepetir.setCheckMarkDrawable(R.drawable.repeat);
                            }

                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                chkRepetir.setChecked(isShow);
                                chkRepetir.setTextColor(Color.parseColor("#9E9E9E"));
                                chkRepetir.setCheckMarkDrawable(R.drawable.not_repeat);
                            }

                        });
        popDialog.create().show();
    }

}
