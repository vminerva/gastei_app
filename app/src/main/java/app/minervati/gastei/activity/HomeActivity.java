package app.minervati.gastei.activity;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeBounds;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import app.minervati.gastei.R;
import app.minervati.gastei.fragment.DespesasFragment;
import app.minervati.gastei.fragment.HomeFragment;
import app.minervati.gastei.fragment.MetaFragment;
import app.minervati.gastei.fragment.SaudeFinFragment;
import app.minervati.gastei.util.GasteiUtil;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    /**
     * Constentes
     */
    public final Integer DURATION_TRANSITION = 1000;
    /**
     * Variaveis da tela
     */
    private DrawerLayout            drawer;
    private Toolbar                 toolbar;
    private Fragment                fragment = null;
    private NavigationView          navigationView;
    private FloatingActionButton    fab;
    private ActionBarDrawerToggle   toggle;
    private FrameLayout             flContent;
    /**
     * Variaveis do sistema
     */
    private Class                   fragmentClass   = null;
    private int                     mStackLevel     = 1;
    public  int []                  location        = new int[2];
    private TransitionInflater      inflater;
    private Transition              transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TRANSITIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inflater = TransitionInflater.from(this);
            transition = inflater.inflateTransition(R.transition.category_test);

            getWindow().setSharedElementExitTransition( transition );
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();

        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        defineFragmentCurrent();
        /* Ação do botão flutuante para cadastrar despesas*/
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {*//*
                Intent i = new Intent(HomeActivity.this, ManterDespesaActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
                *//*}else{
                    view.getLocationOnScreen(location);
                    int cx = (location[0] + (view.getWidth() / 2));
                    int cy = location[1] + (getStatusBarHeight(view.getContext()) / 2);

                    View decorView = getWindow().getDecorView();
                    int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
                    decorView.setSystemUiVisibility(uiOptions);
                    showRevealEffect(revealView, cx, cy, revealAnimationListener);
                }*//*
            }
        });*/
    }

    private void init(){
        toolbar             = (Toolbar) findViewById(R.id.toolbar);
        drawer              = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView      = (NavigationView) findViewById(R.id.nav_view);
        fragmentClass       = HomeFragment.class;
        //fab                 = (FloatingActionButton) findViewById(R.id.add);
        flContent           = (FrameLayout) findViewById(R.id.flContent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    /* Evento quando clica no botão voltar */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /* Evento quando clica em algum dos itens do navigation drawer */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.nav_resumo:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.nav_despesas:
                fragmentClass = DespesasFragment.class;
                break;
            case R.id.nav_saude_fin:
                fragmentClass = SaudeFinFragment.class;
                break;
            case R.id.nav_meta:
                fragmentClass = MetaFragment.class;
                break;
            case R.id.nav_config:
                //fragmentClass = ThirdFragment.class;
                break;
            case R.id.nav_help:
                //fragmentClass = ThirdFragment.class;
                break;
            default:
                fragmentClass = HomeFragment.class;
        }

        defineFragmentCurrent();
        drawer.closeDrawers();

        return true;
    }

    /**
     * Método que gerencia os fragments da tela
     */
    private void defineFragmentCurrent() {
        try {
            if(!GasteiUtil.isNull(fragmentClass))
                fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        //fab.setVisibility(View.VISIBLE);
    }

    /* Animação do botão flutuante  */
    Animator.AnimatorListener revealAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {}
        @Override
        public void onAnimationEnd(Animator animation) {
            Intent i = new Intent(HomeActivity.this, ManterDespesaActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
            overridePendingTransition(0, 0);
            finish();
        }
        @Override
        public void onAnimationCancel(Animator animation) {}
        @Override
        public void onAnimationRepeat(Animator animation) {}
    };
    public static int getStatusBarHeight(Context c) {
        int result = 0;
        int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = c.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void showRevealEffect(final View v, int centerX, int centerY, Animator.AnimatorListener lis) {
        v.setVisibility(View.VISIBLE);
        int height = v.getHeight();
        Animator anim = ViewAnimationUtils.createCircularReveal(v, centerX, centerY, 0, height);
        anim.setDuration(350);
        anim.addListener(lis);
        anim.start();
    }
}
