package app.minervati.gastei.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.fabtransitionactivity.SheetLayout;

import app.minervati.gastei.R;
import app.minervati.gastei.model.TipoCategoriaEnum;
import app.minervati.gastei.util.GasteiUtil;

public class CategoriaActivity extends AppCompatActivity implements SheetLayout.OnFabAnimationEndListener {

    /**
     * Constantes
     */
    public final Integer        DURATION_TRANSITION = 1000;
    private static final int    REQUEST_CODE = 1;
    /**
     * Variaveis da tela
     */
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private ViewGroup mRoot;

    /**
     * Variaveis do sistema
     */
    private TransitionInflater inflater;
    private Transition transition;
    private Integer position;
    private Bundle extras;
    private SheetLayout mSheetLayout;

    /**
     *

     mSheetLayout.setFab(mFab);
     mSheetLayout.setFabAnimationEndListener
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TRANSITIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inflater = TransitionInflater.from(this);
            transition = inflater.inflateTransition(R.transition.category_enter);

            getWindow().setSharedElementExitTransition(transition);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoria);
        init();
        //ButterKnife.bind(this);

        if(!GasteiUtil.isNull(extras)) {
            position = extras.getInt("position");
            defineDetailsToolbar(toolbar);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSheetLayout.setFabAnimationEndListener(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSheetLayout.expandFab();
                /*Intent intent = new Intent(view.getContext(), ManterDespesaActivity.class);
                startActivity(intent);*/
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }

    public void init(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.add);
        extras = getIntent().getExtras();
        mRoot = (ViewGroup) findViewById(R.id.fragment_categ);
        mSheetLayout = (SheetLayout) findViewById(R.id.bottom_sheet);
        mSheetLayout.setFab(fab);
    }

    @Override
    public void onFabAnimationEnd() {
        Intent intent = new Intent(this, ManterDespesaActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            mSheetLayout.contractFab();
        }
    }
    //TODO
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
        }
        return true;
    }
    /**
     * Método que distingue o layout quando é realizado o click em alguma das categorias
     * @param toolbar
     */
    private void defineDetailsToolbar(Toolbar toolbar) {
        switch (position) {
            case 0:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_pink_primary, this),
                        TipoCategoriaEnum.C.getValor(),
                        GasteiUtil.getColor(R.color.theme_pink_text, this),
                        getResources().getColorStateList(R.color.theme_pink_primary),
                        GasteiUtil.getColor(R.color.theme_pink_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_pink_primary, this));
                break;
            case 1:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_blue_primary, this),
                        TipoCategoriaEnum.S.getValor(),
                        GasteiUtil.getColor(R.color.theme_blue_text, this),
                        getResources().getColorStateList(R.color.theme_blue_primary),
                        GasteiUtil.getColor(R.color.theme_blue_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_blue_primary, this));
                break;
            case 2:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_green_primary, this),
                        TipoCategoriaEnum.A.getValor(),
                        GasteiUtil.getColor(R.color.theme_green_text, this),
                        getResources().getColorStateList(R.color.theme_green_primary),
                        GasteiUtil.getColor(R.color.theme_green_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_green_primary, this));

                break;
            case 3:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_grey_primary, this),
                        TipoCategoriaEnum.L.getValor(),
                        GasteiUtil.getColor(R.color.theme_grey_text, this),
                        getResources().getColorStateList(R.color.theme_grey_primary),
                        GasteiUtil.getColor(R.color.theme_grey_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_grey_primary, this));
                break;
            case 4:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_purple_primary, this),
                        TipoCategoriaEnum.O.getValor(),
                        GasteiUtil.getColor(R.color.theme_purple_text, this),
                        getResources().getColorStateList(R.color.theme_purple_primary),
                        GasteiUtil.getColor(R.color.theme_purple_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_purple_primary, this));
                break;
            case 5:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.colorPrimary, this),
                        TipoCategoriaEnum.CT.getValor(),
                        GasteiUtil.getColor(R.color.theme_blue_text, this),
                        getResources().getColorStateList(R.color.colorPrimary),
                        GasteiUtil.getColor(R.color.colorPrimaryDark, this),
                        GasteiUtil.getColor(R.color.colorPrimary, this));
                break;
            case 6:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_yellow_primary, this),
                        TipoCategoriaEnum.E.getValor(),
                        GasteiUtil.getColor(R.color.theme_yellow_text, this),
                        getResources().getColorStateList(R.color.theme_yellow_primary),
                        GasteiUtil.getColor(R.color.theme_yellow_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_yellow_primary, this));
                break;
            case 7:
                detailsToolbar(toolbar, GasteiUtil.getColor(R.color.theme_red_primary, this),
                        TipoCategoriaEnum.M.getValor(),
                        GasteiUtil.getColor(R.color.theme_red_text, this),
                        getResources().getColorStateList(R.color.theme_red_primary),
                        GasteiUtil.getColor(R.color.theme_red_primary_dark, this),
                        GasteiUtil.getColor(R.color.theme_red_primary, this));
                break;
            default:
                toolbar.setBackgroundColor(GasteiUtil.getColor(R.color.colorPrimary, this));
        }
    }

    /**
     * Método que define o layout da activity categoria
     * @param toolbar
     * @param colorBackground
     * @param valor
     * @param colorText
     * @param colorStateList
     * @param colorStatusBar
     */
    private void detailsToolbar(Toolbar toolbar, int colorBackground, String valor, int colorText, ColorStateList colorStateList, int colorStatusBar, int colorFab) {
        toolbar.setBackgroundColor(colorBackground);
        toolbar.setTitle(valor);
        toolbar.setTitleTextColor(colorText);
        fab.setBackgroundTintList(colorStateList);
        mSheetLayout.setColor(colorFab);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getWindow().setStatusBarColor(colorStatusBar);
    }

    private void detailToolbar(){

    }
}
