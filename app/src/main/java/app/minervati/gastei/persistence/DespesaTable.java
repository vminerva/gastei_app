package app.minervati.gastei.persistence;

import android.provider.BaseColumns;

/**
 * Created by victorminerva on 25/03/2016.
 */
public interface DespesaTable {

    String NAME = "despesa";

    String COLUMN_ID = BaseColumns._ID;
    String COLUMN_DESCRICAO = "descricao";
    String COLUMN_VALOR = "valor";
    String COLUMN_CATEGORIA = "categoria";

    String[] PROJECTION = new String[]{COLUMN_ID, COLUMN_DESCRICAO,
            COLUMN_VALOR, COLUMN_CATEGORIA};

    String CREATE = "CREATE TABLE " + NAME + " ("
            + COLUMN_ID + " TEXT PRIMARY KEY, "
            + COLUMN_DESCRICAO + " TEXT NOT NULL, "
            + COLUMN_VALOR + " TEXT NOT NULL, "
            + COLUMN_CATEGORIA + " CATEGORY NOT NULL);";
}
