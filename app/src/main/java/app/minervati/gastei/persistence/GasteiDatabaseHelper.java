package app.minervati.gastei.persistence;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by victorminerva on 25/03/2016.
 */
public class GasteiDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "gastei";
    private static final String DB_SUFFIX = ".db";
    private static final int DB_VERSION = 1;

    private static GasteiDatabaseHelper mInstance;
    private final Resources mResources;


    public GasteiDatabaseHelper(Context context) {
        super(context, DB_NAME + DB_SUFFIX, null, DB_VERSION);
        mResources = context.getResources();
    }

    private static GasteiDatabaseHelper getInstance(Context context) {
        if (null == mInstance) {
            mInstance = new GasteiDatabaseHelper(context);
        }
        return mInstance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
