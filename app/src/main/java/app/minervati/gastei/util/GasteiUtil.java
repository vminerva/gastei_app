package app.minervati.gastei.util;

import android.app.Activity;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;

/**
 * Created by victorminerva on 19/03/2016.
 */
public class GasteiUtil {

    public static Boolean isNull(Object obj){
        Boolean isNull = false;
        if(null == obj)
            isNull = true;

        return isNull;
    }

    public static Boolean isNullOrEmpty(Object obj){
        Boolean isNull = false;
        if(null == obj || "".equals(obj))
            isNull = true;

        return isNull;
    }

    public static int getColor(@ColorRes int colorRes, Activity mActivity) {
        return ContextCompat.getColor(mActivity, colorRes);
    }

}
