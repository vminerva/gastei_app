package app.minervati.gastei.adapter;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

import app.minervati.gastei.R;
import app.minervati.gastei.activity.CategoriaActivity;
import app.minervati.gastei.model.Categoria;
import app.minervati.gastei.model.Theme;
import app.minervati.gastei.util.GasteiUtil;

/**
 * Created by victorminerva on 20/03/2016.
 */
public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.CategoriaViewHolder> {

    private final Activity mActivity;
    private android.app.FragmentManager fragManager;
    private android.app.FragmentTransaction fragmentTransaction;

    public static class CategoriaViewHolder extends RecyclerView.ViewHolder {

        ImageView   icon_categoria;
        TextView    titleCategory;

        public CategoriaViewHolder(View v) {
            super(v);
            icon_categoria  = (ImageView) v.findViewById(R.id.category_icon);
            titleCategory   = (TextView) v.findViewById(R.id.category_title);

        }
    }

    private List<Categoria> categorias;

    public HomeCategoryAdapter(List<Categoria> categorias, Activity activity){
        this.categorias = categorias;
        mActivity       = activity;
    }

    @Override
    public HomeCategoryAdapter.CategoriaViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_categoria_home, viewGroup, false);
        fragManager         = mActivity.getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        CategoriaViewHolder cvh = new CategoriaViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(final HomeCategoryAdapter.CategoriaViewHolder categoriaViewHolder, final int position) {
        Categoria category = categorias.get(position);
        Theme theme = category.getTheme();
        categoriaViewHolder.icon_categoria.setBackgroundColor(GasteiUtil.getColor(theme.getWindowBackgroundColor(), mActivity));
        categoriaViewHolder.icon_categoria.setImageResource(categorias.get(position).getPhotoId());
        categoriaViewHolder.titleCategory.setText(category.getTipo().getValor());
        categoriaViewHolder.titleCategory.setTextColor(GasteiUtil.getColor(theme.getTextPrimaryColor(), mActivity));
        categoriaViewHolder.titleCategory.setBackgroundColor(GasteiUtil.getColor(theme.getPrimaryColor(), mActivity));
        categoriaViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, CategoriaActivity.class);
                intent.putExtra("position", position);

                //TRANSITIONS
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    View categoryItem = v.findViewById(R.id.category_item);
                    View categoryIcon = v.findViewById(R.id.category_icon);
                    View categoryTitle = v.findViewById(R.id.category_title);

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
                            Pair.create(categoryItem, "element_item"),
                            Pair.create(categoryIcon, "element_icon"),
                            Pair.create(categoryTitle, "element_title")
                            );

                    mActivity.startActivity(intent, options.toBundle());
                } else {
                    mActivity.startActivity(intent);
                }
            }
        });

        //Animação das categorias
        try{
            YoYo.with(Techniques.ZoomInDown)
                    .duration(700)
                    .playOn(categoriaViewHolder.itemView);
        }catch (Exception e){}
    }

    @Override
    public long getItemId(int position) {
        return categorias.get(position).getId().hashCode();
    }

    @Override
    public int getItemCount() {
        return categorias.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
